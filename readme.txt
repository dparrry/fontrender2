Setup render.php script along with font files on a separate server and then call the script from shopify shop to return an image based on variants in shopify. 

{% if product.type == "Type" %}
      <div class="fontPreview" style="display:inline-block;text-align:left;">     
        
        <input type="text" name="font_text" id="font-text" placeholder="The five boxing wizards jump quickly"> 
        <div class='font-preview-controls'>
        <span class='font-size-sm'>A</span><input type="range" id="font-size-slider" min="12" max="150" value="20"><span class='font-size-lg'>A</span>
        <span class='font-wordwrap'><input type="checkbox" id="font-wordwrap-checkbox" value="unchecked"> Wrap text</span>
        <input type="button" id="font-preview-button" value="Preview" class="btn small action" data-action="update-font-preview"> 
        </div>

          <ul id="font-preview-list">
          {% for variant in product.variants %}
          {%assign vtitle = variant.title | downcase %} 
          {% if vtitle contains 'complete' %}
          {% else %}
          <li>
          	
            {% if product.variants.size == 1 %}
			  <img src=“URL_TO_YOUR_FILES/render.php?rendertxt=The five boxing wizards jump quickly&fn={{product.id}}&fs=20&imgW=500" alt="fontPreview" data-variant={{product.id}}>
			  <p><h6>{{product.title}}<h6></p>
            {% else %}
              <img src="URL_TO_YOUR_FILES/render.php?rendertxt=The five boxing wizards jump quickly&fn={{variant.id}}&fs=20&imgW=500" alt="fontPreview" data-variant={{variant.id}}>
              <p><h6>{{variant.title}}<h6></p>
            {% endif %}
              
          </li>
          {% endif %}
          {% endfor %}
        </ul>
           
        
      </div>
{% endif %}