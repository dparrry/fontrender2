<?php
// Create a 300x100 image

$type_id_Name_hash = array(
"467343255" => "Milkshake",
"4332628931"=> "this-Regular",
);

 // ini_set('display_errors','On');
 // error_reporting(E_ALL);

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$font_size = $_GET["fs"];
$font_name = $_GET["fn"];
$text_to_render = urldecode($_GET["rendertxt"]);
$img_width = $_GET['imgW'];
$wraptxt = $_GET['wrap'];
$img_height = $font_size*2;

$im = imagecreatetruecolor($img_width, $img_height);

$red = imagecolorallocate($im, 0xFF, 0x00, 0x00);
$black = imagecolorallocate($im, 0x22, 0x22, 0x22);
//$black = imagecolorallocate($im, 0x00, 0x00, 0x00);
$white = imagecolorallocate($im, 0xFf, 0xFf, 0xFf);

$start_x = 10;
$start_y = $font_size;


$font_name = strval($type_id_Name_hash[$font_name]);
$font_file = strval(loadPreviewFont($font_name, "../fonts/".$font_name.".ttf"));

if (empty($wraptxt)){$wraptxt = false;}
if (!$img_width){$img_width = 300;}


// Make the background white
imagefilledrectangle($im, 0, 0, $img_width, $img_height, $white);

if ($font_file !== FALSE){

	$boxcoor = imagettfbbox($font_size, 0, $font_file, $text_to_render);
	//$start_x = abs($boxcoor[6]);
	$img_height = (abs($boxcoor[3]) + abs($boxcoor[7])) + $start_x*2;
	$start_y = abs($boxcoor[7]) + $start_x;
	$im = imagecreatetruecolor($img_width, $img_height);
	imagefilledrectangle($im, 0, 0, $img_width, $img_height, $white);

	// Path to our ttf font file

	// Draw the text array imagefttext ( resource $image , float $size , float $angle , int $x , int $y , int $color , string $fontfile , string $text [, array $extrainfo ] )
	imagefttext($im, $font_size, 0, $start_x, $start_y, $black, $font_file, $text_to_render);
	imagesavealpha($im, false);

	if ($wraptxt){

		$img_height = 2000;
		$im = imagecreatetruecolor($img_width, $img_height);
		imagefilledrectangle($im, 0, 0, $img_width, $img_height, $white);
		$boxcoor = imageftbbox($font_size, 0, $font_file, "Xx");
		$standHeight = (abs($boxcoor[3]) + abs($boxcoor[7]));
		
		$explodedtxt = explode(' ', $text_to_render);
		$numWords = 0;
		$foreachindex = 0; 
		$longestCharCount = 1;
		$currentCharCount = 0;
		
		foreach($explodedtxt as $chunk){
			
			// get coordinates of bounding box containing text
			$boxcoor = imageftbbox($font_size, 0, $font_file, $chunk);	
			
			//$currentCharCount += strlen($chunk);
			if(($start_x+$boxcoor[0] + $boxcoor[4] + 10) > $img_width){
		        $start_x = 5;
		        $start_y += $standHeight+ 20;
				
				if ($currentCharCount >= $longestCharCount){ 
					$longestCharCount = $currentCharCount;
					$currentCharCount = 0;	
					
			    }else if ($numWords == 0){
					$explodedword = str_split( $chunk );
					$wordLen = 0;
					
					foreach($explodedword as $ltr){		
						$boxcoor = imageftbbox($font_size, 0, $font_file, $ltr);
						if(($wordLen+$boxcoor[0] + $boxcoor[4] + 10) > $img_width){
							//print 'in word explode with char count '.$currentCharCount;
							$longestCharCount = $currentCharCount;
							$currentCharCount = 0;
							break;
						}
						$wordLen += ($boxcoor[0] + $boxcoor[4]);
						$currentCharCount += 1;
					}
					$numWords = 0;
				}
				
		    }
		    else {
			
			 $currentCharCount += strlen($chunk) +1;
			//print $currentCharCount."\n";
		     //$image_width = $image_width - 10;
		     //$output .= ($counter % 15 < 1) ? '\r' : '';
		    }
		
		   $numWords += 1;
		   $end_x = $boxcoor[0] + $boxcoor[4] + 10;
			
		   // draw the text chunk
		   //imagefttext($im, $font_size, 0, $start_x, $start_y, $black, $font_file, $chunk);
		
		   // adjust starting coordinates to the END of the just-drawn text
		   $start_x += $end_x;
		}
		
		$longestCharCount = ($longestCharCount == 1 )? strlen($text_to_render) : $longestCharCount;
		
		//if ($longestCharCount > )	
		
		$start_y += $standHeight - 20;
		//$croparray = array('x' =>0 , 'y' => 10, 'width' => $img_width, 'height'=> $start_y);
		//$im = imagecrop($im, $croparray );
		//bool imagecopyresized ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y , int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )
		imagecopyresized($im, $im, 0,0,0,0,$img_width,$start_y,$img_width,$img_height);

		$wrapTest = wordwrap($text_to_render, $longestCharCount, "\n",true );//"lcc = ".$longestCharCount;//
		$boxcoor = imageftbbox($font_size, 0, $font_file, $wrapTest, array("linespacing" => 0.8));
		$start_x = 10;
		$img_height = (abs($boxcoor[3]) + abs($boxcoor[7])) + $start_x*2;
		$start_y = abs($boxcoor[7]) + $start_x;
		$im = imagecreatetruecolor($img_width, $img_height);
		imagefilledrectangle($im, 0, 0, $img_width, $img_height, $white);
		imagefttext($im, $font_size, 0, $start_x, $start_y, $black, $font_file, $wrapTest, array("linespacing" => 0.8));
		
		
	}

}else{
	$boxcoor = imageftbbox($font_size, 0, "../fonts/Affair.otf", $text_to_render);
	$img_height = (abs($boxcoor[3]) + abs($boxcoor[7])) + $start_x*2;
	$start_y = abs($boxcoor[7]) + $start_x;
	
	imagefttext($im, $font_size, 0, $start_x, $start_y, $black, "../fonts/Affair.otf", "Preview not available");
}
// function to check for the otf version if the ttf isn't found.


function getCharLenForWrap($ffile, $fsize, $maxW){
	$boxcoor = imageftbbox($fsize, 0, $ffile, 'XxxX');
	//print intval($maxW/(abs($boxcoor[2])/4) );
	return intval($maxW/($boxcoor[2] - $boxcoor[0])/4 );
}

function loadPreviewFont($load_fontname, $load_fontfile ){
	
		if ( !file_exists($load_fontfile) ){
			$load_fontfile = "../fonts/".$load_fontname.'.otf';		
			if (!file_exists($load_fontfile) ){
				return FALSE;
			}else{
				return $load_fontfile;
			}
		}else{
			return $load_fontfile;
		}
}

// Output image to the browser
header('Content-Type: image/png');
//header($_SERVER['SERVER_PROTOCOL'] . '200 OK');

imagepng($im);
imagedestroy($im);



?>